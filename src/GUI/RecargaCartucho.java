/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Controladores.ControladorCartucho;
import Controladores.ControladorDepartamento;
import Controladores.ControladorFuncionario;
import Controladores.ControladorRecarga;
import Entidades.Cartucho;
import Entidades.Departamento;
import Entidades.Funcionario;
import java.util.Calendar;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Recepcion
 */
public class RecargaCartucho extends javax.swing.JFrame {
    
    List<Cartucho> codigosCartucho;
    List<Departamento> codigosDepartamento;
    List<Funcionario> codigosFuncionario;
    
    Calendar calendario = Calendar.getInstance();
    String dia = String.valueOf(calendario.get(Calendar.DATE));
    String mes = String.valueOf(calendario.get(Calendar.MONTH) + 1);
    String anho = String.valueOf(calendario.get(Calendar.YEAR));
    
    /**
     * Creates new form EntradaCartucho
     */
    public RecargaCartucho() {
        initComponents();
        setLocationRelativeTo(null);
        descripcionTextField.setEditable(false);
        funcionarioComboBox.setEnabled(false);
        if(Integer.parseInt(dia) < 10) dia = '0' + dia;
        if(Integer.parseInt(mes) < 10) mes = '0' + mes;
        fechaTextField.setText(dia + '/' + mes + '/' + anho);
        
        ControladorCartucho ct = new ControladorCartucho();
        
        codigosCartucho = ct.listar();
        
        while(!codigosCartucho.isEmpty()) {
            
            codigoComboBox.addItem(codigosCartucho.get(0).getCodigo());
        
            codigosCartucho.remove(0);
        }
        
        ControladorDepartamento cd = new ControladorDepartamento();
        
        codigosDepartamento = cd.listar();
        
        while(!codigosDepartamento.isEmpty()) {
            
            departamentoComboBox.addItem(codigosDepartamento.get(0).getDepartamento());
        
            codigosDepartamento.remove(0);
        }
        
        
        setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        tituloLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        codigoLabel = new javax.swing.JLabel();
        descripcionLabel = new javax.swing.JLabel();
        descripcionTextField = new javax.swing.JTextField();
        codigoComboBox = new javax.swing.JComboBox<>();
        guardarButton = new javax.swing.JButton();
        cancelarButton = new javax.swing.JButton();
        departamentoLabel = new javax.swing.JLabel();
        departamentoComboBox = new javax.swing.JComboBox<>();
        funcionarioLabel = new javax.swing.JLabel();
        fechaLabel = new javax.swing.JLabel();
        fechaTextField = new javax.swing.JFormattedTextField();
        cantidadRecarga = new javax.swing.JLabel();
        cantidadRecargaSpinner = new javax.swing.JSpinner();
        funcionarioComboBox = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tituloLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        tituloLabel.setForeground(new java.awt.Color(0, 102, 51));
        tituloLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/RecycleIcon.png"))); // NOI18N
        tituloLabel.setText("Recarga de Cartucho");

        codigoLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        codigoLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/LabelIcon.png"))); // NOI18N
        codigoLabel.setText("Código:");

        descripcionLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        descripcionLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/DescriptionIcon.png"))); // NOI18N
        descripcionLabel.setText("Descripción:");

        descripcionTextField.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N

        codigoComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        codigoComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione un código..." }));
        codigoComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                codigoComboBoxActionPerformed(evt);
            }
        });
        codigoComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                codigoComboBoxKeyPressed(evt);
            }
        });

        guardarButton.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        guardarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/SaveIcon.png"))); // NOI18N
        guardarButton.setText("Guardar");
        guardarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                guardarButtonActionPerformed(evt);
            }
        });
        guardarButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                guardarButtonKeyPressed(evt);
            }
        });

        cancelarButton.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        cancelarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/CancelIcon.png"))); // NOI18N
        cancelarButton.setText("Cancelar");
        cancelarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarButtonActionPerformed(evt);
            }
        });

        departamentoLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        departamentoLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/DepartmentIcon.png"))); // NOI18N
        departamentoLabel.setText("Departamento destino:");

        departamentoComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        departamentoComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione un departamento..." }));
        departamentoComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                departamentoComboBoxActionPerformed(evt);
            }
        });
        departamentoComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                departamentoComboBoxKeyPressed(evt);
            }
        });

        funcionarioLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        funcionarioLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/UserIcon.png"))); // NOI18N
        funcionarioLabel.setText("Funcionario:");

        fechaLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        fechaLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/DateIcon.png"))); // NOI18N
        fechaLabel.setText("Fecha:");

        fechaTextField.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter()));
        fechaTextField.setToolTipText("");

        cantidadRecarga.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        cantidadRecarga.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/RecycleIcon.png"))); // NOI18N
        cantidadRecarga.setText("Cantidad recargada:");

        cantidadRecargaSpinner.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        cantidadRecargaSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));
        cantidadRecargaSpinner.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cantidadRecargaSpinnerKeyPressed(evt);
            }
        });

        funcionarioComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        funcionarioComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione un funcionario..." }));
        funcionarioComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                funcionarioComboBoxActionPerformed(evt);
            }
        });
        funcionarioComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                funcionarioComboBoxKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(182, 182, 182)
                .addComponent(tituloLabel)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cantidadRecarga, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(fechaLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(fechaTextField))
                    .addComponent(departamentoLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(codigoLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(descripcionLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(funcionarioLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(descripcionTextField)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(codigoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(departamentoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cantidadRecargaSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(funcionarioComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(136, 136, 136)
                        .addComponent(guardarButton)
                        .addGap(18, 18, 18)
                        .addComponent(cancelarButton)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tituloLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codigoLabel)
                    .addComponent(codigoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descripcionLabel)
                    .addComponent(descripcionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cantidadRecarga)
                    .addComponent(cantidadRecargaSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(departamentoLabel)
                    .addComponent(departamentoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(funcionarioComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(funcionarioLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 57, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(guardarButton)
                    .addComponent(cancelarButton)
                    .addComponent(fechaTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fechaLabel))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void codigoComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_codigoComboBoxActionPerformed
        
        codigoComboBox.removeItem("Seleccione un código...");
        
        String codigo = (String) codigoComboBox.getSelectedItem();
        
        ControladorCartucho cc = new ControladorCartucho();
        
        Cartucho cartucho = cc.obtener(codigo);
        
        descripcionTextField.setText(cartucho.getDescripcion());
    }//GEN-LAST:event_codigoComboBoxActionPerformed

    private void departamentoComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_departamentoComboBoxActionPerformed
        
        departamentoComboBox.removeItem("Seleccione un departamento...");
        
        funcionarioComboBox.setEnabled(true);
        
        String departamento = (String) departamentoComboBox.getSelectedItem();
        
        ControladorFuncionario cf = new ControladorFuncionario();
        
        codigosFuncionario = cf.listar(departamento);
        
        funcionarioComboBox.removeAllItems();
        
        while(!codigosFuncionario.isEmpty()) {
            
            funcionarioComboBox.addItem(codigosFuncionario.get(0).getNombre());
        
            codigosFuncionario.remove(0);
        }
    }//GEN-LAST:event_departamentoComboBoxActionPerformed

    private void guardarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_guardarButtonActionPerformed
        
        Integer cantidadRecarga;
            
        cantidadRecarga = (Integer) cantidadRecargaSpinner.getValue();
        
        if(cantidadRecarga < 0) JOptionPane.showMessageDialog(this, "La cantidad a dar recarga no puede ser negativa.");
        
        else {
            
            String fecha = fechaTextField.getText();
            
            String codigo = (String) codigoComboBox.getSelectedItem();
            
            String departamento = (String) departamentoComboBox.getSelectedItem();
            
            String nombreFuncionario = (String) funcionarioComboBox.getSelectedItem();
            
            nombreFuncionario = nombreFuncionario.toUpperCase();
            
            ControladorCartucho cc = new ControladorCartucho();
            
            ControladorRecarga cr = new ControladorRecarga();
            
            ControladorFuncionario cf = new ControladorFuncionario();
            
            Funcionario funcionario = cf.obtener(nombreFuncionario);
            
            if(funcionario == null || !funcionario.getDepartamento().equals(departamento)) {
                
                JOptionPane.showMessageDialog(this, "No existe el funcionario.");
                
                return;
            }
            
            Cartucho cartucho = cc.obtener(codigo);
            
            JOptionPane.showMessageDialog(this, cr.guardarRecarga(cartucho, fecha, departamento, nombreFuncionario, cantidadRecarga));
            
            this.dispose();
        }
    }//GEN-LAST:event_guardarButtonActionPerformed

    private void cancelarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarButtonActionPerformed
        
        JOptionPane.showMessageDialog(this, "Operación cancelada.");
        
        this.dispose();
    }//GEN-LAST:event_cancelarButtonActionPerformed

    private void codigoComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_codigoComboBoxKeyPressed
        
        if(evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER)
            departamentoComboBox.requestFocus();
        
        if(evt.getKeyCode() == java.awt.event.KeyEvent.VK_ESCAPE)
            cancelarButton.doClick();
    }//GEN-LAST:event_codigoComboBoxKeyPressed

    private void cantidadRecargaSpinnerKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidadRecargaSpinnerKeyPressed

        if(evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER)
        departamentoComboBox.requestFocus();
        
        if(evt.getKeyCode() == java.awt.event.KeyEvent.VK_ESCAPE)
            cancelarButton.doClick();
    }//GEN-LAST:event_cantidadRecargaSpinnerKeyPressed

    private void departamentoComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_departamentoComboBoxKeyPressed
        
        if(evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER)
            funcionarioComboBox.requestFocus();
        
        if(evt.getKeyCode() == java.awt.event.KeyEvent.VK_ESCAPE)
            cancelarButton.doClick();
    }//GEN-LAST:event_departamentoComboBoxKeyPressed

    private void funcionarioComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_funcionarioComboBoxActionPerformed
        
        funcionarioComboBox.removeItem("Seleccione un funcionario...");
    }//GEN-LAST:event_funcionarioComboBoxActionPerformed

    private void funcionarioComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_funcionarioComboBoxKeyPressed
        
        if(evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER)
            guardarButton.requestFocus();
        
        if(evt.getKeyCode() == java.awt.event.KeyEvent.VK_ESCAPE)
            cancelarButton.doClick();
    }//GEN-LAST:event_funcionarioComboBoxKeyPressed

    private void guardarButtonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_guardarButtonKeyPressed
        
        if(evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER)
            guardarButton.doClick();
        
        if(evt.getKeyCode() == java.awt.event.KeyEvent.VK_ESCAPE)
            cancelarButton.doClick();
    }//GEN-LAST:event_guardarButtonKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RecargaCartucho.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RecargaCartucho.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RecargaCartucho.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RecargaCartucho.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RecargaCartucho().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelarButton;
    private javax.swing.JLabel cantidadRecarga;
    private javax.swing.JSpinner cantidadRecargaSpinner;
    private javax.swing.JComboBox<String> codigoComboBox;
    private javax.swing.JLabel codigoLabel;
    private javax.swing.JComboBox<String> departamentoComboBox;
    private javax.swing.JLabel departamentoLabel;
    private javax.swing.JLabel descripcionLabel;
    private javax.swing.JTextField descripcionTextField;
    private javax.swing.JLabel fechaLabel;
    private javax.swing.JFormattedTextField fechaTextField;
    private javax.swing.JComboBox<String> funcionarioComboBox;
    private javax.swing.JLabel funcionarioLabel;
    private javax.swing.JButton guardarButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel tituloLabel;
    // End of variables declaration//GEN-END:variables
}
