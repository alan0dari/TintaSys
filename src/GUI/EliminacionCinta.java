/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Controladores.ControladorCinta;
import Entidades.Cinta;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Recepcion
 */
public class EliminacionCinta extends javax.swing.JFrame {

    List<Cinta> codigos;
    
    /**
     * Creates new form EliminacionToner
     */
    public EliminacionCinta() {
        initComponents();
        setLocationRelativeTo(null);
        descripcionTextField.setEditable(false);
        stockTextField.setEditable(false);
        
        ControladorCinta ct = new ControladorCinta();
        
        codigos = ct.listar();
        
        while(!codigos.isEmpty()) {
            
            codigoComboBox.addItem(codigos.get(0).getCodigo());
        
            codigos.remove(0);
        }
        
        setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        tituloLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        codigoLabel = new javax.swing.JLabel();
        descripcionLabel = new javax.swing.JLabel();
        stockLabel = new javax.swing.JLabel();
        descripcionTextField = new javax.swing.JTextField();
        stockTextField = new javax.swing.JTextField();
        codigoComboBox = new javax.swing.JComboBox<>();
        cancelarButton = new javax.swing.JButton();
        eliminarButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tituloLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 18)); // NOI18N
        tituloLabel.setForeground(new java.awt.Color(255, 0, 0));
        tituloLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/DeleteIcon.png"))); // NOI18N
        tituloLabel.setText("Eliminación de Cinta");

        codigoLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        codigoLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/LabelIcon.png"))); // NOI18N
        codigoLabel.setText("Código:");

        descripcionLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        descripcionLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/DescriptionIcon.png"))); // NOI18N
        descripcionLabel.setText("Descripción:");

        stockLabel.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 14)); // NOI18N
        stockLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/StockIcon.png"))); // NOI18N
        stockLabel.setText("Cantidad en stock:");

        descripcionTextField.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N

        stockTextField.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N

        codigoComboBox.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        codigoComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione un código..." }));
        codigoComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                codigoComboBoxActionPerformed(evt);
            }
        });
        codigoComboBox.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                codigoComboBoxKeyPressed(evt);
            }
        });

        cancelarButton.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        cancelarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/CancelIcon.png"))); // NOI18N
        cancelarButton.setText("Cancelar");
        cancelarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelarButtonActionPerformed(evt);
            }
        });

        eliminarButton.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        eliminarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icons/DeleteIcon.png"))); // NOI18N
        eliminarButton.setText("Eliminar");
        eliminarButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                eliminarButtonActionPerformed(evt);
            }
        });
        eliminarButton.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                eliminarButtonKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(codigoLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(descripcionLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(stockLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(descripcionTextField)
                            .addComponent(stockTextField)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(codigoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(eliminarButton)
                        .addGap(18, 18, 18)
                        .addComponent(cancelarButton)))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(146, 146, 146)
                .addComponent(tituloLabel)
                .addContainerGap(131, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tituloLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(codigoLabel)
                    .addComponent(codigoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(descripcionLabel)
                    .addComponent(descripcionTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(stockLabel)
                    .addComponent(stockTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelarButton)
                    .addComponent(eliminarButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void codigoComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_codigoComboBoxActionPerformed
        
        codigoComboBox.removeItem("Seleccione un código...");
        
        String codigo = (String) codigoComboBox.getSelectedItem();
        
        ControladorCinta cc = new ControladorCinta();
        
        Cinta cinta = cc.obtener(codigo);
        
        descripcionTextField.setText(cinta.getDescripcion());
        
        stockTextField.setText(String.valueOf(cinta.getStock()));
    }//GEN-LAST:event_codigoComboBoxActionPerformed

    private void eliminarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_eliminarButtonActionPerformed
        
        String codigo = (String) codigoComboBox.getSelectedItem();
        
        ControladorCinta cc = new ControladorCinta();
        
        JOptionPane.showMessageDialog(this, cc.eliminar(codigo));
        
        this.dispose();
    }//GEN-LAST:event_eliminarButtonActionPerformed

    private void cancelarButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelarButtonActionPerformed
        
        JOptionPane.showMessageDialog(this, "Operación cancelada.");
        
        this.dispose();
    }//GEN-LAST:event_cancelarButtonActionPerformed

    private void codigoComboBoxKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_codigoComboBoxKeyPressed
        
        if(evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER)
            eliminarButton.requestFocus();
        
        if(evt.getKeyCode() == java.awt.event.KeyEvent.VK_ESCAPE)
            cancelarButton.doClick();
    }//GEN-LAST:event_codigoComboBoxKeyPressed

    private void eliminarButtonKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_eliminarButtonKeyPressed
        
        if(evt.getKeyCode() == java.awt.event.KeyEvent.VK_ENTER)
            eliminarButton.doClick();
        
        if(evt.getKeyCode() == java.awt.event.KeyEvent.VK_ESCAPE)
            cancelarButton.doClick();
    }//GEN-LAST:event_eliminarButtonKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EliminacionCinta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EliminacionCinta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EliminacionCinta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EliminacionCinta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EliminacionCinta().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelarButton;
    private javax.swing.JComboBox<String> codigoComboBox;
    private javax.swing.JLabel codigoLabel;
    private javax.swing.JLabel descripcionLabel;
    private javax.swing.JTextField descripcionTextField;
    private javax.swing.JButton eliminarButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel stockLabel;
    private javax.swing.JTextField stockTextField;
    private javax.swing.JLabel tituloLabel;
    // End of variables declaration//GEN-END:variables
}
