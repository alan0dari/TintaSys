/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Entidades.Imprimible;
import Entidades.Salida;
import java.awt.print.*;

/**
 *
 * @author ADMIN
 */
public class ControladorImpresion {
    
    public void imprimir(Salida salida) {
        
        PrinterJob job = PrinterJob.getPrinterJob();
        
        job.setPrintable(new Imprimible(salida));
        
        if(job.printDialog()) {
            
            try {

                job.print();
            }
            catch(PrinterException e) {

                System.out.println(e);
            }
        }
    }
}