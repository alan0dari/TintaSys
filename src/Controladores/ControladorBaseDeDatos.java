/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controladores;

import java.sql.*;

public class ControladorBaseDeDatos {
    
    public static void conectar() {
        
        Connection dbConnection = null;
        String strUrl = "jdbc:derby://localhost:1527/TintaSys;create=true";

        try {
            
            dbConnection = DriverManager.getConnection(strUrl);
        }
        catch (SQLException ex) {
            
            ex.printStackTrace();
        }
    }
}