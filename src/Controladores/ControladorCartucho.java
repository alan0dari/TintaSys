/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Entidades.Cartucho;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Recepcion
 */
public class ControladorCartucho {
    
    public String guardar(Cartucho cartucho) {
        
        if(cartucho.getStockColor() < 0 || cartucho.getStockNegro() < 0)
            return "Parámetros inválidos";
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(cartucho.getCodigo()) == null) {
                
                em.persist(cartucho);
                
                em.getTransaction().commit();
            }
            
            else return "Ya existe el cartucho " + cartucho.getCodigo();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al guardar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Cartucho guardado exitosamente";
    }
    
    public String eliminar(String codigo) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(codigo) == null) return "No existe el cartucho " + codigo;
            
            else em.remove(em.find(Cartucho.class, codigo));
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al eliminar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Cartucho eliminado exitosamente";
    }
    
    public String modificar(Cartucho cartucho) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(cartucho.getCodigo()) == null) return "No existe el cartucho " + cartucho.getCodigo();
            
            this.eliminar(cartucho.getCodigo());
            
            em.persist(cartucho);
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al modificar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Cartucho modificado exitosamente.";
    }

    public Cartucho obtener(String codigo) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        Cartucho cartucho = null;
        
        try {
            
            cartucho = em.find(Cartucho.class, codigo);
        }
        catch(NullPointerException e) {
            
            System.out.println("No existe el cartucho con código " + codigo);
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return cartucho;
    }
    
    public List<Cartucho> listar() {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        List<Cartucho> codigos = null;
        
        try {
            
            codigos = em.createNamedQuery("Cartucho.findAll").getResultList();
        }
        catch(Exception e) {}
        
        return codigos;
    }
    
    public List<Cartucho> listarPorStock() {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        List<Cartucho> codigos = null;
        
        try {
            
            codigos = em.createNamedQuery("Cartucho.findAllByStockNegro").getResultList();
        }
        catch(Exception e) {}
        
        return codigos;
    }
    
    public List<Cartucho> listarPorStockBajo() {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        List<Cartucho> codigos = null;
        
        try {
            
            codigos = em.createNamedQuery("Cartucho.findAllByStockNegroBajo").getResultList();
        }
        catch(Exception e) {}
        
        return codigos;
    }
}
