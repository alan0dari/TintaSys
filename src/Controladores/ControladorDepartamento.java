/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Entidades.Departamento;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Recepcion
 */
public class ControladorDepartamento {
    
    public String guardar(Departamento departamento) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(departamento.getDepartamento()) == null) {
                
                em.persist(departamento);
                
                em.getTransaction().commit();
            }
            
            else return "Ya existe el departamento " + departamento.getDepartamento();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al guardar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Departamento guardado exitosamente";
    }
    
    public String eliminar(String departamento) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(departamento) == null) return "No existe el departamento " + departamento;
            
            else em.remove(em.find(Departamento.class, departamento));
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al eliminar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Departamento eliminado exitosamente";
    }
    
    public String modificar(Departamento departamento) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(departamento.getDepartamento()) == null) return "No existe el departamento " + departamento.getDepartamento();
            
            this.eliminar(departamento.getDepartamento());
            
            em.persist(departamento);
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al modificar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Departamento modificado exitosamente.";
    }

    public Departamento obtener(String departamento) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        Departamento depto = null;
        
        try {
            
            depto = em.find(Departamento.class, departamento);
        }
        catch(NullPointerException e) {
            
            System.out.println("No existe el departamento con código " + departamento);
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return depto;
    }
    
    public List<Departamento> listar() {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        List<Departamento> codigos = null;
        
        try {
            
            codigos = em.createNamedQuery("Departamento.findAll").getResultList();
        }
        catch(Exception e) {}
        
        return codigos;
    }
}