/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Entidades.Cinta;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Recepcion
 */
public class ControladorCinta {
    
    public String guardar(Cinta cinta) {
        
        if(cinta.getStock() < 0)
            return "Parámetros inválidos";
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(cinta.getCodigo()) == null) {
                
                em.persist(cinta);
                
                em.getTransaction().commit();
            }
            
            else return "Ya existe la cinta " + cinta.getCodigo();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al guardar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Cinta guardada exitosamente";
    }
    
    public String eliminar(String codigo) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(codigo) == null) return "No existe la cinta " + codigo;
            
            else em.remove(em.find(Cinta.class, codigo));
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al eliminar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Cinta eliminada exitosamente";
    }
    
    public String modificar(Cinta cinta) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(cinta.getCodigo()) == null) return "No existe la cinta " + cinta.getCodigo();
            
            this.eliminar(cinta.getCodigo());
            
            em.persist(cinta);
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al modificar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Cinta modificada exitosamente.";
    }

    public Cinta obtener(String codigo) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        Cinta cinta = null;
        
        try {
            
            cinta = em.find(Cinta.class, codigo);
        }
        catch(NullPointerException e) {
            
            System.out.println("No existe la cinta con código " + codigo);
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return cinta;
    }

    public List<Cinta> listar() {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        List<Cinta> codigos = null;
        
        try {
            
            codigos = em.createNamedQuery("Cinta.findAll").getResultList();
        }
        catch(Exception e) {}
        
        return codigos;
    }
    
    public List<Cinta> listarPorStock() {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        List<Cinta> codigos = null;
        
        try {
            
            codigos = em.createNamedQuery("Cinta.findAllByStock").getResultList();
        }
        catch(Exception e) {}
        
        return codigos;
    }
    
    public List<Cinta> listarPorStockBajo() {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        List<Cinta> codigos = null;
        
        try {
            
            codigos = em.createNamedQuery("Cinta.findAllByStockBajo").getResultList();
        }
        catch(Exception e) {}
        
        return codigos;
    }
}
