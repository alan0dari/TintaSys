/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Entidades.*;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Recepcion
 */
public class ControladorRecarga {
    
    public String guardarRecarga(Cartucho cartucho, String fecha, String departamento, String funcionario, Integer cantidadRecarga) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        Contadores contador = em.find(Contadores.class, "RECARGA");
        
        Calendar fechaCalendar = Calendar.getInstance();
        
        fechaCalendar.set(Integer.parseInt(fecha.substring(6, 10)),
                      Integer.parseInt(fecha.substring(3, 5)) - 1,
                      Integer.parseInt(fecha.substring(0, 2)));
        
        Recarga recarga = new Recarga(contador.getContador() + 1, fechaCalendar, cartucho.getCodigo(), cartucho.getDescripcion(), departamento, funcionario, cantidadRecarga);
        
        ControladorDepartamento cd = new ControladorDepartamento();
        
        Departamento dpto = cd.obtener(departamento);
        
        try {
            
            em.persist(recarga);
            
            contador.setContador(contador.getContador() + 1);
            
            em.persist(contador);
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al guardar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Recarga guardada exitosamente";
    }
    
    public List<Recarga> recargasPorFechas(Calendar desde, Calendar hasta, String codigo, String departamento, String funcionario) {
        
        String consulta = "SELECT r FROM Recarga r WHERE r.fecha BETWEEN :desde AND :hasta";
        
        if(codigo != "Todos los items")
            consulta = consulta + " AND r.codigo = :codigo";
        
        if(departamento != "Todos los departamentos")
            consulta = consulta + " AND r.departamento = :departamento";
        
        if(funcionario != "Todos los funcionarios")
            consulta = consulta + " AND r.funcionario = :funcionario";

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();

        Query query = em.createQuery(consulta);
        
        query.setParameter("desde", desde, TemporalType.DATE);
        
        query.setParameter("hasta", hasta, TemporalType.DATE);
        
        if(codigo != "Todos los items")
            query.setParameter("codigo", codigo);
        
        if(departamento != "Todos los departamentos")
            query.setParameter("departamento", departamento);
            
        if(funcionario != "Todos los funcionarios")
            query.setParameter("funcionario", funcionario);
        
        List<Recarga> lista = query.getResultList();
        
        em.close();
        
        emf.close();
        
        return lista;
    }
    
    public String eliminar(Integer codigo) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            em.remove(em.find(Recarga.class, codigo));
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al eliminar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Recarga eliminada exitosamente";
    }
}
