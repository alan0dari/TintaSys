/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Entidades.*;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Recepcion
 */
public class ControladorEntrada {
    
    public String guardarCartucho(Cartucho cartucho, String fecha, Integer cantidadEntradaNegro, Integer cantidadEntradaColor) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        Contadores contador = em.find(Contadores.class, "ENTRADA");
        
        Calendar fechaCalendar = Calendar.getInstance();
        
        fechaCalendar.set(Integer.parseInt(fecha.substring(6, 10)),
                      Integer.parseInt(fecha.substring(3, 5)) - 1,
                      Integer.parseInt(fecha.substring(0, 2)));
        
        Entrada entrada = new Entrada(contador.getContador() + 1, cartucho.getCodigo(), cartucho.getDescripcion(), fechaCalendar, cantidadEntradaNegro, cantidadEntradaColor);
        
        try {
            
            em.persist(entrada);
            
            contador.setContador(contador.getContador() + 1);
            
            em.persist(contador);
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al guardar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Entrada guardada exitosamente";
    }
    
    public String guardarCinta(Cinta cinta, String fecha, Integer cantidadEntrada) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        Contadores contador = em.find(Contadores.class, "ENTRADA");
        
        Calendar fechaCalendar = Calendar.getInstance();
        
        fechaCalendar.set(Integer.parseInt(fecha.substring(6, 10)),
                      Integer.parseInt(fecha.substring(3, 5)) - 1,
                      Integer.parseInt(fecha.substring(0, 2)));
        
        Entrada entrada = new Entrada(contador.getContador() + 1, cinta.getCodigo(), cinta.getDescripcion(), fechaCalendar, cantidadEntrada, 0);
        
        try {
            
            em.persist(entrada);
            
            contador.setContador(contador.getContador() + 1);
            
            em.persist(contador);
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al guardar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Entrada guardada exitosamente";
    }
    
    public String guardarToner(Toner toner, String fecha, Integer cantidadEntrada) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        Contadores contador = em.find(Contadores.class, "ENTRADA");
        
        Calendar fechaCalendar = Calendar.getInstance();
        
        fechaCalendar.set(Integer.parseInt(fecha.substring(6, 10)),
                      Integer.parseInt(fecha.substring(3, 5)) - 1,
                      Integer.parseInt(fecha.substring(0, 2)));
        
        Entrada entrada = new Entrada(contador.getContador() + 1, toner.getCodigo(), toner.getDescripcion(), fechaCalendar, cantidadEntrada, 0);
        
        try {
            
            em.persist(entrada);
            
            contador.setContador(contador.getContador() + 1);
            
            em.persist(contador);
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al guardar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Entrada guardada exitosamente";
    }
    
    public List<Entrada> entradasPorFechas(Calendar desde, Calendar hasta, String codigo) {
        
        String consulta;
        
        if(codigo == "Todos los items")
            consulta = "SELECT e FROM Entrada e WHERE e.fecha BETWEEN :desde AND :hasta";
        
        else
            consulta = "SELECT e FROM Entrada e WHERE e.codigo = :codigo AND e.fecha BETWEEN :desde AND :hasta";

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();

        Query query = em.createQuery(consulta);
        
        query.setParameter("desde", desde, TemporalType.DATE);
        
        query.setParameter("hasta", hasta, TemporalType.DATE);
        
        if(codigo != "Todos los items")
            query.setParameter("codigo", codigo);
        
        List<Entrada> lista = query.getResultList();
        
        em.close();
        
        emf.close();
        
        return lista;
    }
}
