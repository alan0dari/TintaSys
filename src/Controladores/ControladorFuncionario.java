/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Entidades.Funcionario;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author ADMIN
 */
public class ControladorFuncionario {
    
    public String guardar(Funcionario funcionario) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(funcionario.getNombre()) == null) {
                
                em.persist(funcionario);
                
                em.getTransaction().commit();
            }
            
            else return "Ya existe el funcionario";
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al guardar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Funcionario guardado exitosamente";
    }
    
    public Funcionario obtener(String nombre) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        Funcionario funcionario = null;
        
        try {
            
            funcionario = em.find(Funcionario.class, nombre);
        }
        catch(NullPointerException e) {
            
            System.out.println("No existe el funcionario con nombre " + nombre);
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return funcionario;
    }

    public String eliminar(String nombre) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(nombre) == null) return "No existe el funcionario " + nombre;
            
            else em.remove(em.find(Funcionario.class, nombre));
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al eliminar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Funcionario eliminado exitosamente";
    }
    
    public List<Funcionario> listar(String departamento) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        List<Funcionario> codigos = null;
        
        Query query;
        
        if(departamento == "TODOS")
            query = em.createNamedQuery("Funcionario.findAll");
        
        else {
            query = em.createNamedQuery("Funcionario.findByDepartamento");
            
            query.setParameter("departamento", departamento);
        }
        
        codigos = query.getResultList();

        
        return codigos;
    }
}