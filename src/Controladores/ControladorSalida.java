/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Entidades.*;
import java.util.Calendar;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Recepcion
 */
public class ControladorSalida {
    
    public String guardarCartucho(Cartucho cartucho, String fecha, String departamento, String funcionario, Integer cantidadSalidaNegro, Integer cantidadSalidaColor) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        Contadores contador = em.find(Contadores.class, "SALIDA");
        
        Calendar fechaCalendar = Calendar.getInstance();
        
        fechaCalendar.set(Integer.parseInt(fecha.substring(6, 10)),
                      Integer.parseInt(fecha.substring(3, 5)) - 1,
                      Integer.parseInt(fecha.substring(0, 2)));
        
        Salida salida = new Salida(contador.getContador() + 1, cartucho.getCodigo(), cartucho.getDescripcion(), fechaCalendar, departamento, funcionario, cantidadSalidaNegro, cantidadSalidaColor);
        
        ControladorDepartamento cd = new ControladorDepartamento();
        
        Departamento dpto = cd.obtener(departamento);
        
        dpto.setCantidadcartucho(dpto.getCantidadcartucho() + cantidadSalidaNegro + cantidadSalidaColor);
        
        cd.modificar(dpto);
        
        try {
            
            em.persist(salida);
            
            contador.setContador(contador.getContador() + 1);
            
            em.persist(contador);
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al guardar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Salida guardada exitosamente";
    }
    
    public String guardarCinta(Cinta cinta, String fecha, String departamento, String funcionario, Integer cantidadSalida) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        Contadores contador = em.find(Contadores.class, "SALIDA");
        
        Calendar fechaCalendar = Calendar.getInstance();
        
        fechaCalendar.set(Integer.parseInt(fecha.substring(6, 10)),
                      Integer.parseInt(fecha.substring(3, 5)) - 1,
                      Integer.parseInt(fecha.substring(0, 2)));
        
        Salida salida = new Salida(contador.getContador() + 1, cinta.getCodigo(), cinta.getDescripcion(), fechaCalendar, departamento, funcionario, cantidadSalida, 0);
        
        ControladorDepartamento cd = new ControladorDepartamento();
        
        Departamento dpto = cd.obtener(departamento);
        
        dpto.setCantidadcinta(dpto.getCantidadcinta() + cantidadSalida);
        
        cd.modificar(dpto);
        
        try {
            
            em.persist(salida);
            
            contador.setContador(contador.getContador() + 1);
            
            em.persist(contador);
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al guardar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Salida guardada exitosamente";
    }
    
    public String guardarToner(Toner toner, String fecha, String departamento, String funcionario, Integer cantidadSalida) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        Contadores contador = em.find(Contadores.class, "SALIDA");
        
        Calendar fechaCalendar = Calendar.getInstance();
        
        fechaCalendar.set(Integer.parseInt(fecha.substring(6, 10)),
                      Integer.parseInt(fecha.substring(3, 5)) - 1,
                      Integer.parseInt(fecha.substring(0, 2)));
        
        Salida salida = new Salida(contador.getContador() + 1, toner.getCodigo(), toner.getDescripcion(), fechaCalendar, departamento, funcionario, cantidadSalida, 0);
        
        ControladorDepartamento cd = new ControladorDepartamento();
        
        Departamento dpto = cd.obtener(departamento);
        
        dpto.setCantidadtoner(dpto.getCantidadtoner() + cantidadSalida);
        
        cd.modificar(dpto);
        
        try {
            
            em.persist(salida);
            
            contador.setContador(contador.getContador() + 1);
            
            em.persist(contador);
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al guardar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Salida guardada exitosamente";
    }
    
    public List<Salida> salidasPorFechas(Calendar desde, Calendar hasta, String codigo, String departamento, String funcionario) {
        
        String consulta = "SELECT s FROM Salida s WHERE s.fecha BETWEEN :desde AND :hasta";
        
        if(codigo != "Todos los items")
            consulta = consulta + " AND s.codigo = :codigo";
        
        if(departamento != "Todos los departamentos")
            consulta = consulta + " AND s.departamento = :departamento";
        
        if(funcionario != "Todos los funcionarios")
            consulta = consulta + " AND s.funcionario = :funcionario";

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();

        Query query = em.createQuery(consulta);
        
        query.setParameter("desde", desde, TemporalType.DATE);
        
        query.setParameter("hasta", hasta, TemporalType.DATE);
        
        if(codigo != "Todos los items")
            query.setParameter("codigo", codigo);
        
        if(departamento != "Todos los departamentos")
            query.setParameter("departamento", departamento);
            
        if(funcionario != "Todos los funcionarios")
            query.setParameter("funcionario", funcionario);
        
        List<Salida> lista = query.getResultList();
        
        em.close();
        
        emf.close();
        
        return lista;
    }
    
    public String eliminar(Integer codigo) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            em.remove(em.find(Salida.class, codigo));
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al eliminar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Salida eliminada exitosamente";
    }
}
