/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Entidades.Toner;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Recepcion
 */
public class ControladorToner {
    
    public String guardar(Toner toner) {
        
        if(toner.getStock() < 0)
            return "Parámetros inválidos";
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(toner.getCodigo()) == null) {
                
                em.persist(toner);
                
                em.getTransaction().commit();
            }
            
            else return "Ya existe el toner " + toner.getCodigo();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al guardar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Toner guardado exitosamente";
    }
    
    public String eliminar(String codigo) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(codigo) == null) return "No existe el toner " + codigo;
            
            else em.remove(em.find(Toner.class, codigo));
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al eliminar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Toner eliminado exitosamente";
    }
    
    public String modificar(Toner toner) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(toner.getCodigo()) == null) return "No existe el toner " + toner.getCodigo();
            
            this.eliminar(toner.getCodigo());
            
            em.persist(toner);
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al modificar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Toner modificado exitosamente.";
    }

    public Toner obtener(String codigo) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        Toner toner = null;
        
        try {
            
            toner = em.find(Toner.class, codigo);
        }
        catch(NullPointerException e) {
            
            System.out.println("No existe el toner con código " + codigo);
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return toner;
    }

    public List<Toner> listar() {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        List<Toner> codigos = null;
        
        try {
            
            codigos = em.createNamedQuery("Toner.findAll").getResultList();
        }
        catch(Exception e) {}
        
        return codigos;
    }
    
    public List<Toner> listarPorStock() {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        List<Toner> codigos = null;
        
        try {
            
            codigos = em.createNamedQuery("Toner.findAllByStock").getResultList();
        }
        catch(Exception e) {}
        
        return codigos;
    }
    
    public List<Toner> listarPorStockBajo() {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        List<Toner> codigos = null;
        
        try {
            
            codigos = em.createNamedQuery("Toner.findAllByStockBajo").getResultList();
        }
        catch(Exception e) {}
        
        return codigos;
    }
}