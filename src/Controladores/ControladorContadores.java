/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import Entidades.Contadores;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Recepcion
 */
public class ControladorContadores {
    
    public String crearEntradaYSalida() {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        Contadores entrada = new Contadores("ENTRADA", 0);
        
        Contadores salida = new Contadores("SALIDA", 0);
        
        try {
                
            em.persist(entrada);
            
            em.persist(salida);
                
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al guardar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Contadores guardados exitosamente";
    }

    public Contadores obtener(String codigo) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        Contadores contador = null;
        
        try {
            
            contador = em.find(Contadores.class, codigo);
        }
        catch(NullPointerException e) {
            
            System.out.println("No existe el contador con código " + codigo);
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return contador;
    }
    
    public String modificar(Contadores contador) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(contador.getId()) == null) return "No existe el contador " + contador.getId();
            
            this.eliminar(contador.getId());
            
            em.persist(contador);
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al modificar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Contador modificada exitosamente.";
    }
    
    public String eliminar(String codigo) {
        
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TintaSysPU");
        
        EntityManager em = emf.createEntityManager();
        
        em.getTransaction().begin();
        
        try {
            
            if(this.obtener(codigo) == null) return "No existe el contador " + codigo;
            
            else em.remove(em.find(Contadores.class, codigo));
            
            em.getTransaction().commit();
        }
        catch(Exception e) {
            
            e.printStackTrace();
            
            em.getTransaction().rollback();
            
            return "Ha ocurrido un error inesperado al eliminar.";
        }
        finally {
            
            em.close();
            
            emf.close();
        }
        
        return "Cinta eliminada exitosamente";
    }
}