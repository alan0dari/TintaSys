/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Recepcion
 */
@Entity
@Table(name = "CINTAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cinta.findAll", query = "SELECT c FROM Cinta c ORDER BY c.codigo")
    , @NamedQuery(name = "Cinta.findAllByStock", query = "SELECT c FROM Cinta c ORDER BY c.stock")
    , @NamedQuery(name = "Cinta.findAllByStockBajo", query = "SELECT c FROM Cinta c WHERE c.stock BETWEEN '0' AND '1' ORDER BY c.stock")
    , @NamedQuery(name = "Cinta.findByCodigo", query = "SELECT c FROM Cinta c WHERE c.codigo = :codigo")
    , @NamedQuery(name = "Cinta.findByDescripcion", query = "SELECT c FROM Cinta c WHERE c.descripcion = :descripcion")
    , @NamedQuery(name = "Cinta.findByStock", query = "SELECT c FROM Cinta c WHERE c.stock = :stock")})
public class Cinta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private String codigo;
    @Basic(optional = false)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "STOCK")
    private int stock;

    public Cinta() {
    }

    public Cinta(String codigo) {
        this.codigo = codigo;
    }

    public Cinta(String codigo, String descripcion, int stock) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.stock = stock;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cinta)) {
            return false;
        }
        Cinta other = (Cinta) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.codigo;
    }
}
