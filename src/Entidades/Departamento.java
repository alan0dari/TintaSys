/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name = "DEPARTAMENTOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Departamento.findAll", query = "SELECT d FROM Departamento d ORDER BY d.departamento")
    , @NamedQuery(name = "Departamento.findByDepartamento", query = "SELECT d FROM Departamento d WHERE d.departamento = :departamento")
    , @NamedQuery(name = "Departamento.findByCantidadtoner", query = "SELECT d FROM Departamento d WHERE d.cantidadtoner = :cantidadtoner")
    , @NamedQuery(name = "Departamento.findByCantidadcartucho", query = "SELECT d FROM Departamento d WHERE d.cantidadcartucho = :cantidadcartucho")
    , @NamedQuery(name = "Departamento.findByCantidadcinta", query = "SELECT d FROM Departamento d WHERE d.cantidadcinta = :cantidadcinta")})
public class Departamento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "DEPARTAMENTO")
    private String departamento;
    @Basic(optional = false)
    @Column(name = "CANTIDADTONER")
    private int cantidadtoner;
    @Basic(optional = false)
    @Column(name = "CANTIDADCARTUCHO")
    private int cantidadcartucho;
    @Basic(optional = false)
    @Column(name = "CANTIDADCINTA")
    private int cantidadcinta;

    public Departamento() {
    }

    public Departamento(String departamento) {
        this.departamento = departamento;
    }

    public Departamento(String departamento, int cantidadtoner, int cantidadcartucho, int cantidadcinta) {
        this.departamento = departamento;
        this.cantidadtoner = cantidadtoner;
        this.cantidadcartucho = cantidadcartucho;
        this.cantidadcinta = cantidadcinta;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public int getCantidadtoner() {
        return cantidadtoner;
    }

    public void setCantidadtoner(int cantidadtoner) {
        this.cantidadtoner = cantidadtoner;
    }

    public int getCantidadcartucho() {
        return cantidadcartucho;
    }

    public void setCantidadcartucho(int cantidadcartucho) {
        this.cantidadcartucho = cantidadcartucho;
    }

    public int getCantidadcinta() {
        return cantidadcinta;
    }

    public void setCantidadcinta(int cantidadcinta) {
        this.cantidadcinta = cantidadcinta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (departamento != null ? departamento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Departamento)) {
            return false;
        }
        Departamento other = (Departamento) object;
        if ((this.departamento == null && other.departamento != null) || (this.departamento != null && !this.departamento.equals(other.departamento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.departamento;
    }
    
}
