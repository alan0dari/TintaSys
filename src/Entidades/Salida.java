/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Recepcion
 */
@Entity
@Table(name = "SALIDAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Salida.findAll", query = "SELECT s FROM Salida s ORDER BY s.id")
    , @NamedQuery(name = "Salida.findById", query = "SELECT s FROM Salida s WHERE s.id = :id")
    , @NamedQuery(name = "Salida.findByCodigo", query = "SELECT s FROM Salida s WHERE s.codigo = :codigo")
    , @NamedQuery(name = "Salida.findByDescripcion", query = "SELECT s FROM Salida s WHERE s.descripcion = :descripcion")
    , @NamedQuery(name = "Salida.findByFecha", query = "SELECT s FROM Salida s WHERE s.fecha = :fecha")
    , @NamedQuery(name = "Salida.findByDepartamento", query = "SELECT s FROM Salida s WHERE s.fecha = :fecha")
    , @NamedQuery(name = "Salida.findByFuncionario", query = "SELECT s FROM Salida s WHERE s.fecha = :fecha")
    , @NamedQuery(name = "Salida.findByCantidadNegro", query = "SELECT s FROM Salida s WHERE s.cantidadNegro = :cantidadNegro")
    , @NamedQuery(name = "Salida.findByCantidadColor", query = "SELECT s FROM Salida s WHERE s.cantidadColor = :cantidadColor")})
public class Salida implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Nº")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private String codigo;
    @Basic(optional = false)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @Column(name="FECHA")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar fecha;
    @Basic(optional = false)
    @Column(name = "DEPARTAMENTO")
    private String departamento;
    @Basic(optional = false)
    @Column(name = "FUNCIONARIO")
    private String funcionario;
    @Basic(optional = false)
    @Column(name = "CANTIDADNEGRO")
    private Integer cantidadNegro;
    @Basic(optional = false)
    @Column(name = "CANTIDADCOLOR")
    private Integer cantidadColor;

    public Salida() {
    }

    public Salida(Integer id, String codigo, String descripcion, Calendar fecha, String departamento, String funcionario, Integer cantidadNegro, Integer cantidadColor) {
        this.id = id;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.departamento = departamento;
        this.funcionario = funcionario;
        this.cantidadNegro = cantidadNegro;
        this.cantidadColor = cantidadColor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(String funcionario) {
        this.funcionario = funcionario;
    }

    public Integer getCantidadNegro() {
        return cantidadNegro;
    }

    public void setCantidadNegro(Integer cantidadNegro) {
        this.cantidadNegro = cantidadNegro;
    }

    public Integer getCantidadColor() {
        return cantidadColor;
    }

    public void setCantidadColor(Integer cantidadColor) {
        this.cantidadColor = cantidadColor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Salida)) {
            return false;
        }
        Salida other = (Salida) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.valueOf(this.id);
    }
    
}
