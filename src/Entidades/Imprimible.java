/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.awt.*;
import java.awt.print.*;

/**
 *
 * @author ADMIN
 */
public class Imprimible implements Printable {
    
    private Salida salida;
    
    public int print(Graphics g, PageFormat f, int pageIndex) {
        
        if(pageIndex == 0) {
            
            int altura = 95 * ((salida.getId() % 10) - 1);
            
            g.drawString(salida.getDepartamento(), 140, 55 + altura);
            
            g.drawString(salida.getDescripcion(), 140, 75 + altura);
            
            g.drawString(salida.getFuncionario(), 140, 95 + altura);
            
            g.drawString("NADA", 470, 55 + altura);
            
            g.drawString(salida.getCantidadNegro().toString(), 430, 74 + altura);
            
            g.drawString(salida.getCantidadColor().toString(), 550, 74 + altura);
            
            return PAGE_EXISTS;
        }
        else return NO_SUCH_PAGE;
    }

    public Imprimible(Salida salida) {
        this.salida = salida;
    }
}
    


