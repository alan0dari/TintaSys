/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Recepcion
 */
@Entity
@Table(name = "CARTUCHOS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cartucho.findAll", query = "SELECT c FROM Cartucho c ORDER BY c.codigo")
    , @NamedQuery(name = "Cartucho.findAllByStockNegro", query = "SELECT c FROM Cartucho c ORDER BY c.stockNegro")
    , @NamedQuery(name = "Cartucho.findAllByStockNegroBajo", query = "SELECT c FROM Cartucho c WHERE c.stockNegro BETWEEN '0' AND '1' ORDER BY c.stockNegro")
    , @NamedQuery(name = "Cartucho.findByCodigo", query = "SELECT c FROM Cartucho c WHERE c.codigo = :codigo")
    , @NamedQuery(name = "Cartucho.findByDescripcion", query = "SELECT c FROM Cartucho c WHERE c.descripcion = :descripcion")
    , @NamedQuery(name = "Cartucho.findByStockNegro", query = "SELECT c FROM Cartucho c WHERE c.stockNegro = :stockNegro")
    , @NamedQuery(name = "Cartucho.findByStockColor", query = "SELECT c FROM Cartucho c WHERE c.stockColor = :stockColor")})
public class Cartucho implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private String codigo;
    @Basic(optional = false)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "STOCK_NEGRO")
    private int stockNegro;
    @Basic(optional = false)
    @Column(name = "STOCK_COLOR")
    private int stockColor;

    public Cartucho() {
    }

    public Cartucho(String codigo) {
        this.codigo = codigo;
    }

    public Cartucho(String codigo, String descripcion, int stockNegro, int stockColor) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.stockNegro = stockNegro;
        this.stockColor = stockColor;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        String oldCodigo = this.codigo;
        this.codigo = codigo;
        changeSupport.firePropertyChange("codigo", oldCodigo, codigo);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        String oldDescripcion = this.descripcion;
        this.descripcion = descripcion;
        changeSupport.firePropertyChange("descripcion", oldDescripcion, descripcion);
    }

    public int getStockNegro() {
        return stockNegro;
    }

    public void setStockNegro(int stockNegro) {
        int oldStockNegro = this.stockNegro;
        this.stockNegro = stockNegro;
        changeSupport.firePropertyChange("stockNegro", oldStockNegro, stockNegro);
    }

    public int getStockColor() {
        return stockColor;
    }

    public void setStockColor(int stockColor) {
        int oldStockColor = this.stockColor;
        this.stockColor = stockColor;
        changeSupport.firePropertyChange("stockColor", oldStockColor, stockColor);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cartucho)) {
            return false;
        }
        Cartucho other = (Cartucho) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.codigo;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
}
