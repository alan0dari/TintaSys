/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Recepcion
 */
@Entity
@Table(name = "ENTRADAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Entrada.findAll", query = "SELECT e FROM Entrada e ORDER BY e.id")
    , @NamedQuery(name = "Entrada.findById", query = "SELECT e FROM Entrada e WHERE e.id = :id")
    , @NamedQuery(name = "Entrada.findByCodigo", query = "SELECT e FROM Entrada e WHERE e.codigo = :codigo")
    , @NamedQuery(name = "Entrada.findByDescripcion", query = "SELECT e FROM Entrada e WHERE e.descripcion = :descripcion")
    , @NamedQuery(name = "Entrada.findByFecha", query = "SELECT e FROM Entrada e WHERE e.fecha = :fecha")
    , @NamedQuery(name = "Entrada.findByCantidadNegro", query = "SELECT e FROM Entrada e WHERE e.cantidadNegro = :cantidadNegro")
    , @NamedQuery(name = "Entrada.findByCantidadColor", query = "SELECT e FROM Entrada e WHERE e.cantidadColor = :cantidadColor")})
public class Entrada implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Nº")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "CODIGO")
    private String codigo;
    @Basic(optional = false)
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Basic(optional = false)
    @Column(name="FECHA")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar fecha;
    @Basic(optional = false)
    @Column(name = "CANTIDADNEGRO")
    private Integer cantidadNegro;
    @Basic(optional = false)
    @Column(name = "CANTIDADCOLOR")
    private Integer cantidadColor;

    public Entrada() {
    }

    public Entrada(Integer id, String codigo, String descripcion, Calendar fecha, Integer cantidadNegro, Integer cantidadColor) {
        this.id = id;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.cantidadNegro = cantidadNegro;
        this.cantidadColor = cantidadColor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    public Integer getCantidadNegro() {
        return cantidadNegro;
    }

    public void setCantidadNegro(Integer cantidadNegro) {
        this.cantidadNegro = cantidadNegro;
    }

    public Integer getCantidadColor() {
        return cantidadColor;
    }

    public void setCantidadColor(Integer cantidadColor) {
        this.cantidadColor = cantidadColor;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Entrada)) {
            return false;
        }
        Entrada other = (Entrada) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return String.valueOf(this.id);
    }
    
}
