/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ADMIN
 */
@Entity
@Table(name = "RECARGAS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Recarga.findAll", query = "SELECT r FROM Recarga r")
    , @NamedQuery(name = "Recarga.findById", query = "SELECT r FROM Recarga r WHERE r.id = :id")
    , @NamedQuery(name = "Recarga.findByFecha", query = "SELECT r FROM Recarga r WHERE r.fecha = :fecha")
    , @NamedQuery(name = "Recarga.findByCodigo", query = "SELECT r FROM Recarga r WHERE r.codigo = :codigo")
    , @NamedQuery(name = "Recarga.findByDescripcion", query = "SELECT r FROM Recarga r WHERE r.descripcion = :descripcion")
    , @NamedQuery(name = "Recarga.findByDepartamento", query = "SELECT r FROM Recarga r WHERE r.departamento = :departamento")
    , @NamedQuery(name = "Recarga.findByFuncionario", query = "SELECT r FROM Recarga r WHERE r.funcionario = :funcionario")
    , @NamedQuery(name = "Recarga.findByCantidad", query = "SELECT r FROM Recarga r WHERE r.cantidad = :cantidad")})
public class Recarga implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "Nº")
    private Integer id;
    @Column(name = "FECHA")
    @Temporal(TemporalType.DATE)
    private Calendar fecha;
    @Column(name = "CODIGO")
    private String codigo;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "DEPARTAMENTO")
    private String departamento;
    @Column(name = "FUNCIONARIO")
    private String funcionario;
    @Column(name = "CANTIDAD")
    private Integer cantidad;

    public Recarga() {
    }

    public Recarga(Integer id, Calendar fecha, String codigo, String descripcion, String departamento, String funcionario, Integer cantidad) {
        this.id = id;
        this.fecha = fecha;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.departamento = departamento;
        this.funcionario = funcionario;
        this.cantidad = cantidad;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Calendar getFecha() {
        return fecha;
    }

    public void setFecha(Calendar fecha) {
        this.fecha = fecha;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(String funcionario) {
        this.funcionario = funcionario;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recarga)) {
            return false;
        }
        Recarga other = (Recarga) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return String.valueOf(this.id);
    }
    
}
