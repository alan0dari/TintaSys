package Entidades;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-07T10:51:39")
@StaticMetamodel(Cartucho.class)
public class Cartucho_ { 

    public static volatile SingularAttribute<Cartucho, String> descripcion;
    public static volatile SingularAttribute<Cartucho, String> codigo;
    public static volatile SingularAttribute<Cartucho, Integer> stockNegro;
    public static volatile SingularAttribute<Cartucho, Integer> stockColor;

}