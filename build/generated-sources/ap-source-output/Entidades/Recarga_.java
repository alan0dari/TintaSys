package Entidades;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-07T09:41:04")
@StaticMetamodel(Recarga.class)
public class Recarga_ { 

    public static volatile SingularAttribute<Recarga, String> descripcion;
    public static volatile SingularAttribute<Recarga, Calendar> fecha;
    public static volatile SingularAttribute<Recarga, String> codigo;
    public static volatile SingularAttribute<Recarga, String> departamento;
    public static volatile SingularAttribute<Recarga, Integer> id;
    public static volatile SingularAttribute<Recarga, String> funcionario;
    public static volatile SingularAttribute<Recarga, Integer> cantidad;

}