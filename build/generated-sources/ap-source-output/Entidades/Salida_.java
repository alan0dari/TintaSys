package Entidades;

import java.util.Calendar;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-25T11:45:53")
@StaticMetamodel(Salida.class)
public class Salida_ { 

    public static volatile SingularAttribute<Salida, String> descripcion;
    public static volatile SingularAttribute<Salida, Calendar> fecha;
    public static volatile SingularAttribute<Salida, Integer> cantidadNegro;
    public static volatile SingularAttribute<Salida, String> codigo;
    public static volatile SingularAttribute<Salida, Integer> cantidadColor;
    public static volatile SingularAttribute<Salida, String> departamento;
    public static volatile SingularAttribute<Salida, Integer> id;
    public static volatile SingularAttribute<Salida, String> funcionario;

}